# from redis import StrictRedis
# import logging
#
#
# # Config类
# class Config(object):
#     """配置信息"""
#     DEBUG = True
#
#     SECRET_KEY = 'iECgbYWReMNxkRprrzMo5KAQYnb2UeZ3bwvReTSt+VSESW0OB8zbglT+6rEcDW9X'
#     #数据库的配置信息
#     #配置数据库连接
#     SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/information27'
#     SQLALCHEMY_TRACK_MODIFICATIONS = False
#
#     #redis 配置
#     REDIS_HOST = "127.0.0.1"
#     REDIS_PORT = 6379
#
#     # Session保存配置
#     SESSION_TYPE = 'redis'
#     # 开启session签名
#     SESSION_USE_SIGNER = True
#     # 指定 Session 保存的 redis
#     SESSION_REDIS = StrictRedis(host=REDIS_HOST,port=REDIS_PORT)
#     # 设置需要过期
#     SESSION_PERMANENT = False
#     #设置过期时间
#     PERMANENT_SESSION_LIFETIME = 86400 * 2
#
#
#
# class DevelopmentConfig(Config):
#     """开发环境配置"""
#     DEBUG = True
#     LOG_LEVEL = logging.DEBUG
#
# class ProductionConfig(Config):
#     """生产环境配置"""
#     DEBUG = False
#     LOG_LEVEL = logging.WARNING
#
# class TestingConfig(Config):
#     """测试环境配置"""
#     DEBUG = True
#     TESING = True
#
#
# config = {
#     "development":DevelopmentConfig,
#     "production":ProductionConfig,
#     "testing":TestingConfig
# }
#


import redis
import logging




#定义配置类
class Config(object):
    """工程配置信息"""
    DEBUG = True
    SECRET_KEY = "EjpNVSNQTyGi1VvWECj9TvC/+kq3oujee2kTfQUs8yCM6xX9Yjq52v54g+HVoknA"
    #配置数据库
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/python27_news"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    #配置redis
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379
    # flask_session的配置信息
    SESSION_TYPE = "redis" # 指定 session 保存到 redis 中
    SESSION_USE_SIGNER = True    # 让 cookie 中的 session_id 被加密签名处理
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST,port=REDIS_PORT) # 使用 redis 的实例
    PERMANENT_SESSION_LIFETIME = 86400 * 2  # session 的有效期，单位是秒


class DevelopmentConfig(Config):
    """开发环境配置"""
    DEBUG = True
    LOG_LEVEL = logging.DEBUG


class ProductionConfig(Config):
    """生产环境配置"""
    DEBUG = True
    LOG_LEVEL = logging.WARNING


class TesingConfig(Config):
    """测试环境配置"""
    DEBUG = True
    LOG_LEVEL = logging.DEBUG


# 定义配置字典
config = {
    "development":DevelopmentConfig,
    "production":ProductionConfig,
    "tesing":TesingConfig
}




